            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar">
                <!-- Sidebar Menu -->
                <ul class="nav sidebar-menu">
                    <!--Dashboard-->
                    <li>
                        <a href="../index.php">
                            <i class="menu-icon glyphicon glyphicon-home"></i>
                            <span class="menu-text"> Home </span>
                        </a>
                    </li>
                    <li>
                        <a href="index.php">
                            <i class="menu-icon glyphicon glyphicon-user"></i>
                            <span class="menu-text"> <?= $_SESSION['name']; ?> </span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-gear"></i>
                            <span class="menu-text"> Setting </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?profile=<?= $_SESSION['id']; ?>">
                                    <span class="menu-text">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="../logout.php">
                                    <span class="menu-text">Logout</span>
                                </a>
                            </li>
                        </ul>
                    </li>   
                                        <!--  Admin -->
                    <?php 
                        if (isset($_SESSION['access'])) {
                            if ($_SESSION['access'] == 'admin') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-user"></i>
                            <span class="menu-text"> User </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?users=admin">
                                    <span class="menu-text">Admin</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=kepala sekolah">
                                    <span class="menu-text">Kepala Sekolah</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=staff tu">
                                    <span class="menu-text">Staff Tata Usaha</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=staff keuangan">
                                    <span class="menu-text">Staff Keuangan</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=staff perpustakaan">
                                    <span class="menu-text">Staff Perpustakaan</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=guru">
                                    <span class="menu-text">Guru</span>
                                </a>
                            </li>
                            <li>
                                <a href="?users=wali murid">
                                    <span class="menu-text">Wali Murid</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-graduation-cap"></i>
                            <span class="menu-text"> Akademik </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?users=siswa">
                                    <span class="menu-text">Siswa</span>
                                </a>
                            </li>
                            <li>
                                <a href="?akademik=kelas">
                                    <span class="menu-text">Kelas</span>
                                </a>
                            </li>
                            <li>
                                <a href="?akademik=tahun">
                                    <span class="menu-text">Tahun ajaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="?akademik=pelajaran">
                                    <span class="menu-text">Mata Pelajaran</span>
                                </a>
                            </li>
                             <li>
                                <a href="?akademik=#">
                                    <span class="menu-text">Jadwal Pelajaran</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-signal"></i>
                            <span class="menu-text">Nilai </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=input">
                                    <span class="menu-text">Input Nilai</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=absensi">
                                    <span class="menu-text">Absensi</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">Tampilkan Nilai</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">E-Raport</span>
                                </a>
                            </li>
                        </ul>
                    </li>                
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-tasks"></i>
                            <span class="menu-text"> Modul Pelajaran </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?modul=download">
                                    <span class="menu-text">Download</span>
                                </a>
                            </li>
                            <li>
                                <a href="?modul=upload">
                                    <span class="menu-text">Upload</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-money"></i>
                            <span class="menu-text"> Keuangan </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Input Data Pemasukan</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Input Data Pengeluaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Laporan Keuangan</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text"> Perpustakaan</span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Peminjaman Buku</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Pengembalian Buku</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Data Buku</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                                    <!-- Guru -->
                    <?php
                            }elseif ($_SESSION['access'] == 'guru') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-signal"></i>
                            <span class="menu-text"> Nilai </span>
                            <i class="menu-expand"></i>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="?nilai=input">
                                    <span class="menu-text">Input Nilai</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=absensi">
                                    <span class="menu-text">Absensi</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">Tampilkan Nilai</span>
                                </a>
                            </li>
                             <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">E-Raport</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-tasks"></i>
                            <span class="menu-text"> Modul Pelajaran </span>

                            <i class="menu-expand"></i>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="?modul=download">
                                    <span class="menu-text">Download</span>
                                </a>
                            </li>
                            <!-- <li>
                                <a href="?modul=upload">
                                    <span class="menu-text">Upload</span>
                                </a>
                            </li> -->
                        </ul>
                    </li>

                                    <!-- Wali Murid -->
                    <?php
                            }elseif ($_SESSION['access'] == 'wali murid') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-graduation-cap"></i>
                            <span class="menu-text"> Akademik </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">

                             <li>
                                <a href="?akademik=#">
                                    <span class="menu-text">Jadwal Pelajaran</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-signal"></i>
                            <span class="menu-text"> Nilai </span>

                            <i class="menu-expand"></i>
                        </a>
                        <ul class="submenu">
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">Tampilkan Nilai</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">E-Raport</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                                    <!-- Staff TU -->
                    <?php
                            }elseif ($_SESSION['access'] == 'staff tu') {
                    ?>

                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-graduation-cap"></i>
                            <span class="menu-text"> Akademik </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">

                             <li>
                                <a href="?users=guru">
                                    <span class="menu-text">Guru</span>
                                </a>
                            </li>
                             <li>
                                <a href="?users=siswa">
                                    <span class="menu-text">Siswa</span>
                                </a>
                            </li>
                           <!--  <li>
                                <a href="?akademik=kelas">
                                    <span class="menu-text">Kelas</span>
                                </a>
                            </li>
                            <li>
                                <a href="?akademik=tahun">
                                    <span class="menu-text">Tahun ajaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="?akademik=pelajaran">
                                    <span class="menu-text">Mata Pelajaran</span>
                                </a>
                            </li> -->
                             <li>
                                <a href="?akademik=#">
                                    <span class="menu-text">Jadwal Pelajaran</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-tasks"></i>
                            <span class="menu-text"> Modul Pelajaran </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?modul=download">
                                    <span class="menu-text">Download</span>
                                </a>
                            </li>
                            <li>
                                <a href="?modul=upload">
                                    <span class="menu-text">Upload</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                                        <!-- Staff Keuangan  -->
                    <?php
                            }elseif ($_SESSION['access'] == 'staff keuangan') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-money"></i>
                            <span class="menu-text"> Keuangan </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Input Data Pemasukan</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Input Data Pengeluaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Laporan Keuangan</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                                 <!-- Staff Perpustakaan -->
                    <?php
                            }elseif ($_SESSION['access'] == 'staff perpustakaan') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-book"></i>
                            <span class="menu-text"> Perpustakaan </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Peminjaman Buku</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Pengembalian Buku</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Data Buku</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                                        <!-- Kepala Sekolah  -->
                    <?php
                            }elseif ($_SESSION['access'] == 'kepala sekolah') {
                    ?>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-graduation-cap"></i>
                            <span class="menu-text"> Akademik </span>
                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                             <li>
                                <a href="?akademik=#">
                                    <span class="menu-text">Jadwal Pelajaran</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-signal"></i>
                            <span class="menu-text"> Nilai </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">Tampilkan Nilai</span>
                                </a>
                            </li>
                            <li>
                                <a href="?nilai=tampil">
                                    <span class="menu-text">E-Raport</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-money"></i>
                            <span class="menu-text">Keuangan </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?nilai=#">
                                    <span class="menu-text">Laporan Keuangan</span>
                                </a>
                            </li>
                        </ul>
                    </li>                          
                    <li>
                        <a href="#" class="menu-dropdown">
                            <i class="menu-icon fa fa-tasks"></i>
                            <span class="menu-text"> Modul Pelajaran </span>

                            <i class="menu-expand"></i>
                        </a>

                        <ul class="submenu">
                            <li>
                                <a href="?modul=download">
                                    <span class="menu-text">Download</span>
                                </a>
                            </li>
                        </ul>
                    </li>
    
                    <?php
                            }
                        }
                    ?>                   
                </ul>
                <!-- /Sidebar Menu -->
            </div>
            <!-- /Page Sidebar -->