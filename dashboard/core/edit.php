<?php
	//Admin Edit 
	if (isset($_GET['admin-edit'])) {
		$id 	=	$_GET['admin-edit'];

		if (isset($_POST['admin-update'])) {
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];

			$admin 		=	mysqli_query($conn, "UPDATE users 
											SET `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin'
											WHERE id = '$id'");
			if ($admin) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=admin'>";
			}
		}

		$dataadmin 		= 	mysqli_query($conn, "SELECT * FROM users WHERE id=$id");
		$row 			= 	mysqli_fetch_array($dataadmin);
	}
?>
<?php 
	//Guru Edit
	if (isset($_GET['guru-edit'])) {
		$id 	=	$_GET['guru-edit'];

		if (isset($_POST['guru-update'])) {
			$nip 		=	$_POST['nip'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];
			$kelas 		=	$_POST['kelas'];

			$guru 		=	mysqli_query($conn, "UPDATE users 
											SET `nomor_induk` = '$nip', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = '$kelas'
											WHERE id = '$id'");
			if ($guru) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=guru'>";
			}
		}

		$dataguru		= 	mysqli_query($conn, "SELECT *, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 			= 	mysqli_fetch_array($dataguru);
	}
?>
<?php 
	//Siswa Edit
	if (isset($_GET['siswa-edit'])) {
		$id 	=	$_GET['siswa-edit'];

		if (isset($_POST['siswa-update'])) {
			$nis 		=	$_POST['nis'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];
			$kelas 		=	$_POST['kelas'];

			$siswa 		=	mysqli_query($conn, "UPDATE users 
											SET `nomor_induk` = '$nis', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = '$kelas'
											WHERE id = '$id'");
			if ($siswa) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=siswa'>";
			}
		}

		$datasiswa		= 	mysqli_query($conn, "SELECT *, kelas.kelas_id, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 			= 	mysqli_fetch_array($datasiswa);
	}
?>

<?php 
	//Kepsek edit
	if (isset($_GET['kepsek-edit'])) {
		$id 	=	$_GET['kepsek-edit'];

		if (isset($_POST['kepsek-update'])) {
			$nip 		=	$_POST['nip'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];

			$kepsek 		=	mysqli_query($conn, "UPDATE users 
								SET `nomor_induk` = '$nip', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = 1
								WHERE id = '$id'");
			if ($kepsek) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=kepala sekolah'>";
			}
		}

		$datakepsek		= 	mysqli_query($conn, "SELECT *, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 			= 	mysqli_fetch_array($datakepsek);
	}
?>

<?php 
	//Staff TU
	if (isset($_GET['stafftu-edit'])) {
		$id 	=	$_GET['stafftu-edit'];

		if (isset($_POST['stafftu-update'])) {
			$nip 		=	$_POST['nip'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];

			$kepsek 		=	mysqli_query($conn, "UPDATE users 
								SET `nomor_induk` = '$nip', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = 2
								WHERE id = '$id'");
			if ($kepsek) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=staff tu'>";
			}
		}

		$datastafftu		= 	mysqli_query($conn, "SELECT *, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 				= 	mysqli_fetch_array($datastafftu);
	}
?>

<?php 
	//Staff Keuangan
	if (isset($_GET['staffkeuangan-edit'])) {
		$id 	=	$_GET['staffkeuangan-edit'];

		if (isset($_POST['staffkeuangan-update'])) {
			$nip 		=	$_POST['nip'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];

			$staffkeuangan 		=	mysqli_query($conn, "UPDATE users 
								SET `nomor_induk` = '$nip', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = 3
								WHERE id = '$id'");
			if ($staffkeuangan) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=staff keuangan'>";
			}
		}

		$datastaffkeuangan		= 	mysqli_query($conn, "SELECT *, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 					= 	mysqli_fetch_array($datastaffkeuangan);
	}
?>

<?php 
	//Staff Perpustakaan
	if (isset($_GET['staffperpustakaan-edit'])) {
		$id 	=	$_GET['staffperpustakaan-edit'];

		if (isset($_POST['staffperpustakaan-update'])) {
			$nip 		=	$_POST['nip'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];

			$staffperpustakaan 		=	mysqli_query($conn, "UPDATE users 
								SET `nomor_induk` = '$nip', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = 4
								WHERE id = '$id'");
			if ($staffperpustakaan) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=staff perpustakaan'>";
			}
		}

		$datastaffperpustakaan		= 	mysqli_query($conn, "SELECT *, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 						= 	mysqli_fetch_array($datastaffperpustakaan);
	}
?>

<?php 
	//Wali Murid Edit
	if (isset($_GET['walimurid-edit'])) {
		$id 	=	$_GET['walimurid-edit'];

		if (isset($_POST['walimurid-update'])) {
			$nis 		=	$_POST['nis'];
			$username 	=	$_POST['username'];
			$name 		=	$_POST['name'];
			$telp 		=	$_POST['telp'];
			$status 	=	$_POST['status'];
			$alamat 	=	$_POST['alamat'];
			$kelamin 	=	$_POST['jenis_kelamin'];
			$kelas 		=	$_POST['kelas'];

			$walimurid 		=	mysqli_query($conn, "UPDATE users 
											SET `nomor_induk` = '$nis', `name` = '$name', `username` = '$username', `telp` = '$telp', 
												`alamat` = '$alamat', `status` = '$status', `jenis_kelamin` = '$kelamin', `kelas_id` = '$kelas'
											WHERE id = '$id'");
			if ($walimurid) {
				echo "<meta http-equiv='refresh' content='0;URL=?users=wali murid'>";
			}
		}

		$datawalimurid		= 	mysqli_query($conn, "SELECT *, kelas.kelas_id, kelas.kelas_nama FROM users INNER JOIN kelas ON users.kelas_id=kelas.kelas_id WHERE id=$id");
		$row 				= 	mysqli_fetch_array($datawalimurid);
	}
?>

<?php 
	//Kelas Edit
	if (isset($_GET['kelas-edit'])) {
		$id 	=	$_GET['kelas-edit'];

		if (isset($_POST['kelas-update'])) {
			$kelasnama 	=	$_POST['kelas'];

			$kelas 		=	mysqli_query($conn, "UPDATE kelas SET `kelas_nama` = '$kelasnama' WHERE kelas_id = '$id'");
			if ($kelas) {
				echo "<meta http-equiv='refresh' content='0;URL=?akademik=kelas'>";
			}
		}

		$datakelas		= 	mysqli_query($conn, "SELECT * FROM kelas WHERE kelas_id=$id");
		$row 			= 	mysqli_fetch_array($datakelas);
	}
?>
<?php 
	//Tahun Ajaran Edit
	if (isset($_GET['tahun-edit'])) {
		$id 	=	$_GET['tahun-edit'];

		if (isset($_POST['tahun-update'])) {
			$tahunnama 	=	$_POST['tahun'];

			$tahun 		=	mysqli_query($conn, "UPDATE tahun SET `tahun_nama` = '$tahunnama' WHERE tahun_id = '$id'");
			if ($tahun) {
				echo "<meta http-equiv='refresh' content='0;URL=?akademik=tahun'>";
			}
		}

		$datatahun		= 	mysqli_query($conn, "SELECT * FROM tahun WHERE tahun_id=$id");
		$row 			= 	mysqli_fetch_array($datatahun);
	}
?>
<?php 
	//Mata Pelajaran Edit
	if (isset($_GET['pelajaran-edit'])) {
		$id 	=	$_GET['pelajaran-edit'];

		if (isset($_POST['pelajaran-update'])) {
			$pelajarannama 	=	$_POST['pelajaran'];

			$pelajaran 		=	mysqli_query($conn, "UPDATE pelajaran SET `pelajaran_nama` = '$pelajarannama' WHERE pelajaran_id = '$id'");
			if ($pelajaran) {
				echo "<meta http-equiv='refresh' content='0;URL=?akademik=pelajaran'>";
			}
		}

		$datapelajaran	= 	mysqli_query($conn, "SELECT * FROM pelajaran WHERE pelajaran_id=$id");
		$row 			= 	mysqli_fetch_array($datapelajaran);
	}
?>
<?php 
	//Kategori Edit
	if (isset($_GET['kategori-edit'])) {
		$id 	=	$_GET['kategori-edit'];

		if (isset($_POST['kategori-update'])) {
			$kategorinama 	=	$_POST['nama'];
			$kategorideskripsi 	=	$_POST['deskripsi'];

			$kategori 		=	mysqli_query($conn, "UPDATE kategori SET `kategori_nama` = '$kategorinama', `kategori_deskripsi` = '$kategorideskripsi' WHERE kategori_id = '$id'");
			if ($kategori) {
				echo "<meta http-equiv='refresh' content='0;URL=?artikel=kategori'>";
			}
		}

		$datakategori	= 	mysqli_query($conn, "SELECT * FROM kategori WHERE kategori_id=$id");
		$row 			= 	mysqli_fetch_array($datakategori);
	}
?>
<?php 
	//Data Sekolah Create
	if (isset($_GET['sekolah-edit'])) {
		$id 	=	$_GET['sekolah-edit'];

		if (isset($_POST['sekolah-update'])) {
			$nama		=	$_POST['nama'];
			$alamat		=	$_POST['alamat'];
			$telp		=	$_POST['telp'];
			$visi		=	$_POST['visi'];
			$misi		=	$_POST['misi'];

			$sekolah 	= 	mysqli_query($conn, "UPDATE sekolah 
										SET `sekolah_nama` = '$nama', `sekolah_alamat` = '$alamat', `sekolah_telp` = '$telp', `sekolah_visi` = '$visi', `sekolah_misi` = '$misi' 
										WHERE sekolah_id = '$id'");

			if ($sekolah) {
				echo "<meta http-equiv='refresh' content='0;URL=?akademik=sekolah'>";
			}
		}

		$datasekolah 	=	mysqli_query($conn, "SELECT * FROM sekolah WHERE sekolah_id=$id");
		$row 			=	mysqli_fetch_array($datasekolah);
	}
?>
<?php 
	//Nilai Edit
	if (isset($_GET['nilai-edit'])) {
		$id 	=	$_GET['nilai-edit'];

		if (isset($_POST['nilai-update'])) {
			$kkm 		=	$_POST['kkm'];
			$nilaipoin 	=	$_POST['poin'];

			$nilai 		=	mysqli_query($conn, "UPDATE nilai SET `nilai_kkm` = '$kkm', `nilai_poin` = '$nilaipoin' WHERE nilai_id = '$id'");
			if ($nilai) {
				echo "<meta http-equiv='refresh' content='0;URL=?nilai=tampil'>";
			}
		}

		$datanilai	= 	mysqli_query($conn, "SELECT * FROM nilai WHERE nilai_id=$id");
		$row 			= 	mysqli_fetch_array($datanilai);
	}
?>
<?php 
	//Data Sekolah Create
	if (isset($_GET['artikel-edit'])) {
		$id 	=	$_GET['artikel-edit'];

		if (isset($_POST['update-artikel'])) {
			$judul		=	$_POST['judul'];
			$isi		=	$_POST['isi'];
			$kategori		=	$_POST['kategori'];

			$artikel 	= 	mysqli_query($conn, "UPDATE artikel
										SET `artikel_judul` = '$judul', `artikel_isi` = '$isi', `artikel_tgl` = now(), `kategori_id` = '$kategori' 
										WHERE artikel_id = '$id'");

			if ($artikel) {
				echo "<meta http-equiv='refresh' content='0;URL=?artikel=list'>";
			}
		}

		$dataartikel 	=	mysqli_query($conn, "SELECT * FROM artikel WHERE artikel_id=$id");
		$row 			=	mysqli_fetch_array($dataartikel);
	}
?>
<?php 
	// Profile
	if (isset($_GET['profile'])) {
		$id 	=	$_GET['profile'];

		$dataprofile 	=	mysqli_query($conn, "SELECT * FROM users WHERE id=$id");
		$row 			=	mysqli_fetch_array($dataprofile);
	}
?>
<?php 
	// Profile
	if (isset($_GET['change'])) {
		$id 	=	$_GET['change'];

		if (isset($_POST['change'])) {
			$new		=	$_POST['new'];

			$password 	= 	mysqli_query($conn, "UPDATE users 
										SET `password` = '$new'
										WHERE id = '$id'");

			if ($password) {
				echo "<meta http-equiv='refresh' content='0;URL=index.php'>";
			}
		}

		$datapassword 	=	mysqli_query($conn, "SELECT * FROM users WHERE id=$id");
		$row 			=	mysqli_fetch_array($datapassword);
	}
?>