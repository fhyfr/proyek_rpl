                        <div class="col-xs-12 col-md-12">
                            <div class="well with-header  with-footer">
                                <div class="header bg-blue">
                                    Download Modul Pelajaran
                                </div>
                                <table class="table table-hover">
                                    <thead class="bordered-darkorange">
                                        <tr>
                                            <th width="3%">#</th>
                                            <th>Judul</th>
                                            <th>Tanggal</th>
                                            <th>Type Files</th>
                                            <th>Ukuran Files</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <?php
                                            $sql = mysqli_query($conn, "SELECT * FROM download ORDER BY tanggal_upload DESC");
                                            if(mysqli_num_rows($sql) > 0){
                                                $no = 1;
                                                while($data = mysqli_fetch_assoc($sql)){
                                        ?>
                                                    <tr bgcolor="#fff">
                                                        <td><?= $no; ?></td>                                                        
                                                        <td><?= $data['nama_file']; ?></td>
                                                        <td><?= $data['tanggal_upload']; ?></td>
                                                        <td><?= $data['tipe_file']; ?></td>
                                                        <td><?= formatBytes($data['ukuran_file']); ?></td>
                                                        <td>
                                                        <?php 
                                                            if (isset($_SESSION['access'])) {
                                                                if ($_SESSION['access'] == 'admin') {
                                                        ?>
                                                                <a href="<?= $data['file']; ?>" class="btn btn-success">Download</a>
                                                                <a href="?modul-del=<?= $data['id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data tersebut?')";>Delete</a>
                                                        <?php
                                                                }elseif ($_SESSION['access'] == 'guru') {
                                                        ?>
                                                                <a href="<?= $data['file']; ?>" class="btn btn-success">Download</a>
                                                                <a href="?modul-del=<?= $data['id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data tersebut?')";>Delete</a>
                                                        <?php
                                                                }elseif ($_SESSION['access'] == 'siswa') {
                                                        ?>
                                                                <a href="<?= $data['file']; ?>" class="btn btn-success">Download</a>
                                                        <?php
                                                                }
                                                            }
                                                        ?>                                                            
                                                        </td>
                                                    </tr>
                                        <?php
                                                    $no++;
                                                }
                                            }else{
                                                echo '
                                                <tr bgcolor="#fff">
                                                    <td align="center" colspan="4" align="center">Tidak ada data!</td>
                                                </tr>
                                                ';
                                            }
                                        ?>                                                                                
                                    </tbody>
                                </table>
                            </div>
                        </div>
