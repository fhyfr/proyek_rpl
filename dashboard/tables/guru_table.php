                        <div class="col-xs-12 col-md-12">
                            <div class="well with-header  with-footer">
                                <div class="header bg-blue">
                                    Guru
                                </div>
                                <table class="table table-hover">
                                    <thead class="bordered-darkorange">
                                        <tr>
                                            <th>#</th>
                                            <th>NIP</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Telepon</th>
                                            <th>Status</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Kelas</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <?php
                                            $no     =   1; 
                                            $guru  =   mysqli_query($conn, "SELECT users.id, users.nomor_induk, users.name, users.username, users.telp, users.status, 
                                                                    users.alamat, users.jenis_kelamin, kelas.kelas_nama 
                                                                    FROM users 
                                                                    INNER JOIN kelas ON users.kelas_id=kelas.kelas_id 
                                                                    WHERE access='guru' 
                                                                    ORDER BY users.name ASC");

                                            while ($data=mysqli_fetch_array($guru)) {
                                        ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $data['nomor_induk']; ?></td>
                                            <td><?= $data['name']; ?></td>
                                            <td><?= $data['username']; ?></td>
                                            <td><?= $data['telp']; ?></td>
                                            <td><?= $data['status']; ?></td>
                                            <td><?= $data['alamat']; ?></td>
                                            <td><?= $data['jenis_kelamin']; ?></td>
                                            <td><?= $data['kelas_nama']; ?></td>
                                            <td>
                                                <a href="?guru-edit=<?= $data['id']; ?>" class="btn btn-success">Edit</a>
                                                <a href="?guru-del=<?= $data['id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data tersebut?')";>Delete</a>  
                                            </td>
                                        </tr>
                                        <?php
                                            $no++;
                                            }                                            
                                        ?>                                                                                
                                    </tbody>
                                </table>

                                <div class="footer">
                                    <a href="?users=guru-create" class="btn btn-primary">Input</a>
                                </div>
                            </div>
                        </div>