                        <div class="col-xs-12 col-md-12">
                            <div class="well with-header  with-footer">
                                <div class="header bg-blue">
                                    Admin
                                </div>
                                <table class="table table-hover">
                                    <thead class="bordered-darkorange">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Telepon</th>
                                            <th>Status</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <?php
                                            $no     =   1; 
                                            $admin  =   mysqli_query($conn, "SELECT * FROM users WHERE access='admin' ORDER BY name DESC");

                                            while ($data = mysqli_fetch_array($admin)) {
                                        ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $data['name']; ?></td>
                                            <td><?= $data['username']; ?></td>
                                            <td><?= $data['telp']; ?></td>
                                            <td><?= $data['status']; ?></td>
                                            <td><?= $data['alamat']; ?></td>
                                            <td><?= $data['jenis_kelamin']; ?></td>
                                            <td>
                                                <a href="?admin-edit=<?= $data['id']; ?>" class="btn btn-success">Edit</a>
                                                <a href="?admin-del=<?= $data['id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data tersebut?')";>Delete</a>  
                                            </td>
                                        </tr>
                                        <?php
                                            $no++;
                                            }                                            
                                        ?>                                                                                
                                    </tbody>
                                </table>

                                <div class="footer">
                                    <a href="?users=admin-create" class="btn btn-primary">Input</a>
                                </div>
                            </div>
                        </div>