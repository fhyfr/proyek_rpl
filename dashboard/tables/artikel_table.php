                        <div class="col-xs-12 col-md-12">
                            <div class="well with-header  with-footer">
                                <div class="header bg-blue">
                                    Artikel
                                </div>
                                <table class="table table-hover">
                                    <thead class="bordered-darkorange">
                                        <tr>
                                            <th width="3%">#</th>
                                            <th>Judul</th>
                                            <th width="10%">Kategori</th>
                                            <th width="15%">Tanggal</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <?php
                                            $no         =   1; 
                                            $artikel    =   mysqli_query($conn, "SELECT *, kategori.kategori_nama 
                                                                            FROM artikel
                                                                            INNER JOIN kategori ON artikel.kategori_id=kategori.kategori_id 
                                                                            ORDER BY artikel_tgl DESC");

                                            while ($data=mysqli_fetch_array($artikel)) {
                                        ?>
                                        <tr>
                                            <td><?= $no; ?></td>
                                            <td><?= $data['artikel_judul']; ?></td>
                                            <td><?= $data['kategori_nama']; ?></td>
                                            <td><?= $data['artikel_tgl']; ?></td>
                                            <td>
                                                <a href="?artikel-edit=<?= $data['artikel_id']; ?>" class="btn btn-success">Edit</a>
                                                <a href="?artikel-del=<?= $data['artikel_id']; ?>" class="btn btn-danger" onclick="return confirm('Anda yakin akan menghapus data tersebut?')";>Delete</a>  
                                            </td>
                                        </tr>
                                        <?php
                                            $no++;
                                            }                                            
                                        ?>                                                                                
                                    </tbody>
                                </table>

                                <div class="footer">
                                    <a href="?artikel=artikel-create" class="btn btn-primary">Input</a>
                                </div>
                            </div>
                        </div>