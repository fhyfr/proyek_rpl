<?php 

require 'config/db.php';

if (isset($_POST["register"])){

    if(registrasi($_POST ) > 0) {
        
        echo '<script type="text/javascript">'; 
        echo 'alert("user baru berhasil ditambahkan!");'; 
        echo 'window.location = "login.php";';
        echo '</script>';

    } else {
        echo mysqli_error($conn);
    }
}
?>
<!DOCTYPE Html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Registrasi</title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
  </head>
  <body>
    <header class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php" class="navbar-brand scroll-top logo"><img src="assets/images/logo.png" alt="" style="margin-top:-10px;"> <b>Sistem Informasi Sekolah</b></a>
                </div>
            </nav>
            <!--/.navbar-->
        </div>        
        <!--/.container-->
    </header>
    
    <div class="col-sm-4 col-sm-offset-4 form-login">
    
    <?php
    /* handle error */
    if (isset($_GET['error'])) : ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Warning!</strong> <?=base64_decode($_GET['error']);?>
        </div>
    <?php endif;?>

    <div class="container" >
        <div class="outter-form-login">
            <div class="logo-login">
                <form action="" method="post">
                    <h2 class="text-center" style="color: blue;">Registrasi</h2>
                     <div class="form-group">
                        <input type="text" class="form-control" name="nomor_induk" placeholder="Nomor Induk (Untuk Wali Murid isi angka 1)" id="nomor_induk" autofocus required>
                    </div>

                     <div class="form-group">
                        <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" id="name" required>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin" required>
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" id="username" required>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" id="password" required>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" name="password2" placeholder="Ulangi Password" id="password2" required>
                    </div>

                    <div class="form-group">
                        <input type="tel" class="form-control" name="telp" placeholder="Telepon" id="telp" required>
                    </div>

                    <div class="form-group">
                        <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control" placeholder="Alamat"></textarea>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="status" id="status" required>
                            <option>PNS</option>
                            <option>Honorer</option>
                            <option>Wali Murid</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="form-control" name="access" id="access" required>
                            <option>Guru</option>
                            <option>Wali Murid</option>
                            <option>Staff TU</option>
                            <option>Staff Keuangan</option>
                            <option>Staff Perpustakaan</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary btn-block" name="register">Register</button>
                    
                    <div class="text-center forget">
                        <p><br/>Kembali ke halaman <a href="./login.php">Login</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>