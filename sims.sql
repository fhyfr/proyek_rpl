-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 09:31 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sims`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `artikel_id` int(11) NOT NULL,
  `artikel_judul` varchar(255) NOT NULL,
  `artikel_isi` text NOT NULL,
  `artikel_tgl` datetime NOT NULL,
  `kategori_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`artikel_id`, `artikel_judul`, `artikel_isi`, `artikel_tgl`, `kategori_id`) VALUES
(51, 'aa', '<span>aa<br></span>', '2015-04-29 20:45:44', 3),
(59, 'Asdasdwdcadscacasc', 'Buku GRATIS !!!<span> Teman-teman saya mau bagi buku gratis nih, syarat nya:<br> 1. Like dan share status ini<br> 2. Invite pin saya 53DEA7F1<span><br> 3. Silakan tulis di kolom komentar dengan impian terbesar teman-teman</span></span> Karena buku nya tinggal satu pcs, maka komentar terakhir yang mendapatkan like dari saya yang akan mendapatkan buku ini. Pemenang akan saya umumkan besok siang<br> Selamat mencoba, semoga anda beruntung <br>', '2015-04-29 01:11:11', 1),
(60, 'asda', 'Buku GRATIS !!!<span> Teman-teman saya mau bagi buku gratis nih, syarat nya:<br> 1. Like dan share status ini<br> 2. Invite pin saya 53DEA7F1<span><br> 3. Silakan tulis di kolom komentar dengan impian terbesar teman-teman</span></span> Karena buku nya tinggal satu pcs, maka komentar terakhir yang mendapatkan like dari saya yang akan mendapatkan buku ini. Pemenang akan saya umumkan besok siang<br> Selamat mencoba, semoga anda beruntung <br>', '2015-04-29 01:11:06', 1),
(61, 'Antara Bulpen, Aku, dan Cinta', 'Bagiku hari ini adalah hari yang paling membahagian. Bagaimana tidak,\r\n di hari pertama aku masuk sekolah, aku diajak kenalan oleh seorang \r\nwanita. Sungguh aku malu sekali.\r\nAku tak bisa menolaknya, karena aku adalah anak pindahan dari sekolah\r\n sebelah. Jujur, aku tidak pernah mempunyai teman wanita semenjak SD.\r\nAku bingung harus bagaimana saat dia menjulurkan tangan dan meberitahukan namanya sembari berkata â€œnamaku Dewi, salam kenalâ\r\nDengan polosnya aku juga mengenalkan diriku, Miqdad. Setelah berjabat\r\n tangan dia langsung pergi dan tak memedulikanku. Tapi dengan diriku \r\nsendiri, aku tidak bisa bergerak setelah jabatan itu terlepas. Tubuhku \r\ngemetaran dan aku bingung mau berbuat apa.\r\nAku paksa kaki ini melangkah masuk kelas dan berusaha berbuat sebiasa\r\n mungkin, jangan sampai kekonyolan terlihat banyak murit di kelas. \r\nBisa-bisa aku akan hanya menjadi bahan tertawaan.\r\n\r\nSaat itu, bel berdering menandakan pelajaran pertama dimulai. Aku \r\nduduk dibangku paling belakang, berharap tidak ada yang melihatku. Aku \r\nmasih belum terbiasa dengan murit-murit di sini.\r\nBu guru juga menyuruhku perkenalan di depan kelas. Aku memperkenalkan\r\n diriku singkat dan bergegas menuju tempat dudukku lagi. Aku tak begitu \r\nmemedulikan sekitar, aku hanya menatap buku kosong dan berusaha tidak \r\nmelakukan hal yang membuat perhatian anak-anak menuju padaku.\r\nSeketika itu bu guru membuka materinya dan aku pun melihat ke depan. \r\nTanpa ku sadari, setelah aku melirik sedikit bangku kiriku, ternyata dia\r\n adalah wanita yang mengajakku kenalan tadi.\r\nAku semakin gemetaran, kenapa sih cewek ini kok bisa pas ada di \r\nbangku paling belakang, di sampingku lagi. Kenapa dia gak duduk di depan\r\n saja, kenapa pas ketika aku masuk di hari pertama. Gerutuku dalam hati.\r\nPas guru menyuruh kami untuk mencatat, aku pun merombak tas ku dan \r\nmencari pena yang aku sudah siapkan. Aku merogok tas ku sampai terdalam,\r\n ternyata bulpen yang telah aku siapkan dari tadi malam lupa aku masukan\r\n tas.\r\n\r\nAkhirnya aku meminjam pulpen temanku laki-laki di bangku sebelah.\r\nâ€œbro pinjem bulpen dong, aku lupa gak bawa nih\r\nâ€œwah aku cuma punya satu kata temenku.\r\nAku berusaha meminjam ke bangku depan. Ternyata dia juga gak punya.\r\nMalahan dia menyarankan untuk pinjam ke temen perempuan.\r\n\r\nitu tu pinjem Dewi aja, dia punya banyak bulpen.\r\nWhatt ..?, Sebetulnya hari ini kenapa sih, kenapa coba kok pas \r\nbanget. Ketemu sama cewek la, sebelahan la, sekarang aku harus pinjam \r\nbulpen.\r\nKarena terpaksa aku pinjam kepada cewek yang disebelahku, pun juga \r\naku seorang murit baru, aku tidak mau tiba-tiba dianggap murit yang \r\njelek karena tidak mencatat, dan yah aku bilang kepadanya,\r\nâ€œeh pinjem bulpen dong kataku sambil malu-malu.\r\nâ€œohh bulpen, bentar ya\r\nDia mengeluarkan wadah pensilnya dari samping, dan kulihat ada \r\nmungkin sepuluh bulben yang berbeda warna dan jenisnya. Wih banyak \r\nbanget nih anak bulpennya, kataku dalam hati.\r\nSeketika itu tangannya menjulur memberi bulpen yang warna pink, aku pun langsung menangkapnya. Lalu bilang padanya,\r\nâ€œkok pink yang lain kan ada, itu aja tuh yang hitam\r\nâ€œoh gak suka ya, ya udah sini\r\nDia mengambilnya kembali sambil memberikan yang baru yang aku minta. \r\nSaat itu juga tangannya menjulur dan aku hendak mengambilnya lalu \r\ntangannya kembali sambil mendekap bulpen tersebut.\r\nâ€œkalo ini jangan ah, kan ini pemberian ibu ku dari Malaysa.\r\nâ€œya udah yang lain saja kataku.\r\nDia memberikan yang warna biru, aku pun hendak meraihnya. Tapi lagi-lagi dia menggugurkan niatannya.\r\nâ€œini juga jangan ah, ini juga susah belinya gak ada di sini\r\nAku mulai kesal.\r\nâ€œyang ini aja katanya.\r\nAku pun mengambilnya dengan sedikit malas, tapi sekali lagi, dia \r\nmemberi harapan palsu. Tanganku yang sudah ingin merainya dianya dengan \r\nsergap mendekap pulpen yang hendak iya berikan.\r\nAku benar-benar kesal sekarang.\r\nâ€œaghh.. ya udah yang mana saja napa sih, ini sudah ketinggalan jauh nulisnyaaa..\r\nDia pun tertawa kecil seraya memberikan bulpen yang mana saja dan aku\r\n sudah tak memperhatikan warna yang dia berikan. Aku pun segera menulis \r\nketinggalanku.\r\nSekilas, aku teringat kejadian itu di rumah. Di dalam kamar yang \r\nsunyi sendiri ini, aku membanyangkan kejadian tadi pagi. Menurutku itu \r\nadalah hal terindah sepanjang hidupku.\r\nKring .. Kring .. (Dering hp berbunyi)\r\nAku membuka sms dan membaca pesan tersebut. Di sana bertuliskan\r\nâ€œHai Miq, ini Dewi\r\nHaaaaaa â€¦.???\r\nIni ada apa sih, kenapa cobak kok bisaaa â€¦\r\nDia tahu nomerku dari mana, kita tak membicarakan tentang nomer tadi \r\npagi, aku juga ngobrolnya nggak banyak sama dia. Kenapa sih dia bikin \r\ndegdegan terus, baru saja memikirkannya kok bisa-bisanya ada SMS \r\nlangsung dari dia.\r\nAku pun bingung dan tak membalasnya. Tapi tak lama setelah itu, hp_ku berdering lagi.\r\nâ€œDatang ke sekolahan sekarang!\r\nYa Tuhaann..\r\nIni cobaan macam apaaa??\r\nBaru saja kenal, kok dia sok akrab begitu, apa salahku Ya Tuhann..\r\nKenapa juga aku harus mempercayainya, bisa jadi itu nomer nyasar. \r\nKenapa juga aku harus menuruti perintahnya. Dia bukan siapa-siapaku, \r\nmungkin teman tapi belum cukup akrab, kita juga baru kenal tadi pagi.\r\nTapi karena aku yang tak tega meninggalkan perempun sendiri apa lagi \r\nmalam seperti ini maka aku berangkat ke sekolahan. Kebetulan jarak \r\nrumahku dengan sekolah tak terlalu jauh.\r\nDatanglah aku dan mendapatinya sedang menunggu di depan pintu kelas. \r\nAda kursi panjang di sana dengan cahaya lampu jalan kuning \r\nremang-remang. Sekolahku memang pinggir jalan dan dekat dengan rumah \r\nwarga.\r\nAku menghampirinya.\r\nâ€œnih bukumu yang ketinggalan. Kata dia.\r\nâ€œohh iya makasih jawabku dengan malu-mulu.\r\nAku bingung mau berkata apa, aku pun menjawab dengan ala kadarnya. \r\nTernyata aku baru paham dia mendapatkan nomerku dari buku milikku yang \r\nketinggalan.\r\nâ€œlain kali bawa bulpen, dan jangan sampai meninggalkan buku di meja guru. Karena kau bisa menurunkan citra baiku sebagai ketuaâ€\r\nâ€œhehe, iya maapâ€ jawabku singkat.\r\nMalam itu kita membicarakan banyak hal. Aku juga belajar banyak \r\ndarinya, dari apa saja yang harus aku persiapkan, aku harus membawa apa \r\nsaja, guru mana yang biasanya terlihat seram dan menjengkelkan dan lain \r\nsebagainya.\r\nSejak hari itu aku dan Dewi menjadi teman akrab. Setiap hari kita \r\nsaling menghubungi lewat SMS dan kalian tau apa yang lebih \r\nmembahagiakan?\r\nKami jadian setelah 2 bulan.', '2019-12-04 06:08:31', 2),
(62, 'Jadwal Ujian Tengah Semester', 'Pelaksanaan Ujian Tengah Semester untuk tahun ajaran 2018/2019 akan dilaksanakan pada Senin, 1 Desember 2019.<br>', '2019-12-04 06:11:37', 7);

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `id` int(11) NOT NULL,
  `tanggal_upload` date NOT NULL,
  `nama_file` varchar(100) NOT NULL,
  `tipe_file` varchar(10) NOT NULL,
  `ukuran_file` varchar(20) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id`, `tanggal_upload`, `nama_file`, `tipe_file`, `ukuran_file`, `file`) VALUES
(6, '2015-04-28', 'sdf', 'pdf', '128978', 'files/sdf.pdf'),
(8, '2015-04-28', 'daasd', 'docx', '59103', 'files/daasd.docx'),
(9, '2015-04-28', 'asdasdasdas', 'docx', '59103', 'files/asdasdasdas.docx'),
(10, '2015-05-04', 'o9o', 'pdf', '1213253', 'files/o9o.pdf'),
(11, '2015-05-08', 'asd', 'pdf', '373321', 'files/asd.pdf'),
(17, '2019-12-05', 'Bahasa Inggris', 'docx', '15502', 'files/Bahasa Inggris.docx'),
(18, '2019-12-05', 'sukses', 'pdf', '493535', 'files/sukses.pdf'),
(19, '2019-12-05', 'Fisika', 'pdf', '533013', 'files/FORM MAN.pdf'),
(20, '2019-12-05', 'Agama', 'xls', '82944', 'dashboard/files/Format data sekolah siswa MTsN 26.xls'),
(22, '2019-12-05', 'Matematika', 'pdf', '543466', 'files/Matematika.pdf'),
(23, '2019-12-05', 'Aqidah Akhlak', 'pdf', '41153', 'files/Aqidah Akhlak.pdf'),
(24, '2019-12-05', 'Fiqih', 'pdf', '41153', 'files/Fiqih.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `galeri_id` int(11) NOT NULL,
  `galeri_nama` varchar(100) NOT NULL,
  `galeri_keterangan` text NOT NULL,
  `galeri_link` text NOT NULL,
  `galeri_tgl` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`galeri_id`, `galeri_nama`, `galeri_keterangan`, `galeri_link`, `galeri_tgl`) VALUES
(31, 'Ary', 'Ary', 'uploads/C360_2015-01-03-14-20-25-428.jpg', '0000-00-00 00:00:00'),
(32, 'asd', 'asd', 'uploads/C360_2015-01-03-14-20-52-982.jpg', '0000-00-00 00:00:00'),
(34, 'ggg', 'ggg', 'uploads/C360_2015-01-03-15-51-19-682.jpg', '0000-00-00 00:00:00'),
(36, 's', 'ss', 'uploads/C360_2015-01-03-15-50-31-526.jpg', '0000-00-00 00:00:00'),
(38, 'dfsdf', 'fsdf', 'uploads/C360_2014-07-13-19-27-14-435.jpg', '0000-00-00 00:00:00'),
(39, 'dsfsd', 'fsfsdfsdf', 'uploads/20140826_090837.jpg', '0000-00-00 00:00:00'),
(40, 'bbb', 'bb', 'uploads/20140826_091057.jpg', '0000-00-00 00:00:00'),
(41, 'ASdasdasd', 'asdasd', 'uploads/apple_vnbwfjei.jpg', '2015-03-27 23:02:58');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(50) NOT NULL,
  `kategori_deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_nama`, `kategori_deskripsi`) VALUES
(1, 'Hotnews', 'Berisikan kumpulan berita yang sedang trend'),
(2, 'Cerpen', 'Kumpulan Cerita Pendek'),
(3, 'Puisi', 'Berisikan Puisi hasil karya siswa'),
(7, 'Pengumuman', 'Berisi Pengumuman dari sekolah');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL,
  `kelas_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kelas_id`, `kelas_nama`) VALUES
(1, '1 (Satu)'),
(2, '2 (Dua)'),
(3, '3 (Tiga)'),
(4, '4 (Empat)'),
(5, '5 (Lima)'),
(6, '6 (Enam)');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `nilai_id` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `pelajaran_id` int(11) DEFAULT NULL,
  `tahun_id` int(11) DEFAULT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `nilai_kkm` int(11) DEFAULT NULL,
  `nilai_poin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`nilai_id`, `id`, `pelajaran_id`, `tahun_id`, `semester_id`, `nilai_kkm`, `nilai_poin`) VALUES
(119, 27, 1, 1, 1, 60, 59),
(120, 27, 2, 1, 1, 55, 80),
(122, 25, 1, 1, 1, 60, 88),
(125, 28, 1, 1, 1, 70, 69),
(126, 29, 1, 1, 1, 70, 80),
(127, 28, 1, 1, 1, 89, 56),
(128, 29, 1, 1, 1, 75, 90),
(129, 28, 1, 1, 1, 60, 75),
(130, 29, 1, 1, 1, 60, 76),
(131, 28, 1, 2, 1, 90, 80),
(132, 29, 1, 2, 1, 90, 70),
(133, 25, 2, 2, 1, 70, 80),
(135, 40, 3, 2, 1, 70, 85),
(136, 41, 3, 2, 1, 70, 60),
(138, 25, 3, 2, 1, 70, 75),
(141, 42, 3, 2, 1, 70, 67),
(142, 25, 3, 2, 1, 80, 90),
(143, 40, 3, 2, 1, 80, 95),
(144, 41, 3, 2, 1, 80, 85),
(145, 42, 3, 2, 1, 80, 100),
(146, 25, 3, 2, 1, 70, 90),
(147, 40, 3, 2, 1, 70, 50),
(148, 41, 3, 2, 1, 70, 78),
(149, 42, 3, 2, 1, 70, 70);

-- --------------------------------------------------------

--
-- Table structure for table `pelajaran`
--

CREATE TABLE `pelajaran` (
  `pelajaran_id` int(11) NOT NULL,
  `pelajaran_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelajaran`
--

INSERT INTO `pelajaran` (`pelajaran_id`, `pelajaran_nama`) VALUES
(3, 'Al Quran Hadist'),
(4, 'Akidah Akhlak'),
(5, 'Fiqih'),
(6, 'Sejarah Kebudayaa Islam'),
(8, 'Pendidikan Pancasila dan Kewarganegaraan'),
(9, 'Bahasa Indonesia'),
(10, 'Bahasa Arab'),
(11, 'Matematika'),
(12, 'Ilmu Pengetahuan Alam'),
(13, 'Ilmu Pengetahuan Sosial'),
(14, 'Seni Budaya dan Keterampilan'),
(15, 'Pendidikan Jasmani, Olahraga, dan Kesehatan');

-- --------------------------------------------------------

--
-- Table structure for table `sekolah`
--

CREATE TABLE `sekolah` (
  `sekolah_id` int(11) NOT NULL,
  `sekolah_nama` varchar(100) DEFAULT NULL,
  `sekolah_alamat` text DEFAULT NULL,
  `sekolah_telp` varchar(16) DEFAULT NULL,
  `sekolah_visi` text DEFAULT NULL,
  `sekolah_misi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sekolah`
--

INSERT INTO `sekolah` (`sekolah_id`, `sekolah_nama`, `sekolah_alamat`, `sekolah_telp`, `sekolah_visi`, `sekolah_misi`) VALUES
(1, 'MI An-Nizhomiyah', 'Depok, Jawa Barat', '021-4435-2313', 'Menjadi Sekolah terdepan<br>', 'Menyelenggarakan Pendidikan sesuai dengan kurikulum<br>');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `semester_id` int(11) NOT NULL,
  `semester_nama` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`semester_id`, `semester_nama`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tahun`
--

CREATE TABLE `tahun` (
  `tahun_id` int(11) NOT NULL,
  `tahun_nama` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun`
--

INSERT INTO `tahun` (`tahun_id`, `tahun_nama`) VALUES
(2, '2018/2019'),
(3, '2019/2020');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nomor_induk` varchar(30) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `telp` varchar(16) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `jenis_kelamin` varchar(25) DEFAULT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `access` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nomor_induk`, `name`, `username`, `password`, `telp`, `alamat`, `status`, `jenis_kelamin`, `kelas_id`, `access`) VALUES
(10, NULL, 'Administrator', 'admin', 'admin', '085761167894', 'Bali', 'PNS', 'Laki-laki', NULL, 'admin'),
(25, '56', 'siswa', 'siswa', 'siswa', '0813341414456', 'frghf', 'Aktif', 'Perempuan', 1, 'siswa'),
(26, 'op', 'guru', 'guru', 'guru', 'op', 'op', 'Honorer', 'Laki-laki', 1, 'guru'),
(35, '172809990002001', 'Kepala Sekolah', 'kepsek', '123', '081385505546', 'Bogor, Jawa Barat', 'PNS', 'Laki-laki', 6, 'kepala sekolah'),
(39, '3308', 'Lisa Maharani', 'lisa', '123', '085780043607', 'Ciputat', 'Wali Murid', 'Perempuan', 2, 'wali murid'),
(40, '5-25001', 'Achmad Alfarizi', 'alfarizi', 'alfarizi', '0813341414456', 'Prumpung Sawah Rt.009/004, Cipinang Besar Utara\r\n', 'Aktif', 'Laki-laki', 1, 'siswa'),
(41, '5-25002', 'Adinda Alya Rahma', 'adinda', 'adinda', '08988399279', 'Kp. Pul Kambing Rt.008/002, Jatinegara\r\n', 'Aktif', 'Perempuan', 1, 'siswa'),
(42, '5-25003', 'Agung Prabowo', 'agung', 'agung', '081808480464', 'Kp. Jati Rt.010/001 No.21, Jatinegara Kaum\r\n', 'Aktif', 'Laki-laki', 1, 'siswa'),
(43, '5-25004	', 'Ahmad Shami', 'sami', 'sami', '082310522858', 'Kp. Sumur Rt.013/017 No.28, Klender\r\n', 'Aktif', 'Laki-laki', 2, 'siswa'),
(44, '5-25006	', 'Defan Auli Rahmatillah', 'defan', 'defan', '081281328746', 'Jl. Cipinang Timur Rt.007/011 No.39, Cipinang\r\n', 'Aktif', 'Laki-laki', 2, 'siswa'),
(45, '5-25007	', 'Dinda Azizah', 'adinda', 'adinda', '081295587817', 'Jl. Raya Bekasi Timur Tanah Koja Rt.001/002 No.8B\r\n', 'Aktif', 'Perempuan', 3, 'siswa'),
(46, '5-25008	', 'Dwiana Kartika Nugroho', 'dwiana', 'dwiana', '081385329668', ' ', 'Aktif', 'Perempuan', 3, 'siswa'),
(47, '5-25008', ' Dwiana Kartika Nugroho', 'dwiana', 'dwiana', '081385329668', 'Jl. Pertanian Selatan Rt. 001/003 No. 29, Klender\r\n', 'Aktif', 'Perempuan', 3, 'siswa'),
(48, '5-25009	', 'Elmira Rafa Putri', 'elmira', 'elmira', '081319868155', 'Jl. Bumi Raya VII Rt.002/003 No.45\r\n', 'Aktif', 'Perempuan', 3, 'siswa'),
(49, '5-25010	', 'Ezra Danendra Pratama', 'ezra', 'ezra', '085777664449', 'Jl. H. Ahmad R. Rt. 011/004 No. 38, Pondok Bambu\r\n', 'Aktif', 'Laki-laki', 4, 'siswa'),
(50, '5-25011	', 'Faddila Afrita', 'faddila', 'faddila', '081287139720', 'Jl. Cipinang Lontar Rt. 008/006 No. 9, Cipinang Muara\r\n', 'Aktif', 'Perempuan', 4, 'siswa'),
(51, '5-25012	', 'Farid Hanafi', 'farid', 'farid', '081284936323', 'Jl. Cipinang Muara III Rt.014/004 No.32A, Cipinang Muara\r\n', 'Aktif', 'Laki-laki', 4, 'siswa'),
(52, '5-25013	', 'Fathan Hawari', 'fathan', 'fathan', '08128953210', 'Komp. LP Cipinang Rt. 010/014 No. 27\r\n', 'Aktif', 'Laki-laki', 5, 'siswa'),
(53, '5-25014	', 'Faturohman', 'fatur', 'fatur', '081297638759', 'Jl. Cipinang Muara Rt. 001/011, Cipinang Muara\r\n', 'Aktif', 'Laki-laki', 5, 'siswa'),
(54, '5-25015	', 'Fikri Anggana Pratama', 'fikri', 'fikri', '081318396379', 'Jl. Kp. Sumur Rt.012/017 No.110, Klender\r\n', 'Aktif', 'Laki-laki', 4, 'siswa'),
(55, '5-25016	', 'Firda Melati Kalista  Murtiningsih Sucipto', 'firda', 'firda', '081316312544', 'Jl. Cipinang Muara III Rt.006/015 No.26\r\n', 'Aktif', 'Perempuan', 6, 'siswa'),
(56, '5-25017	', 'Gladis Widya Haryanto', 'gladis', 'gladis', '081298642762', 'Kp. Sumur Rt.007/010 No.53, Klender\r\n', 'Aktif', 'Perempuan', 6, 'siswa'),
(57, '5-25018	', 'Hega Rizky Putu Yasa', 'hega', 'hega', '081286431115', 'Jl. Kp. Baru Rt.004/001 No.36\r\n', 'Aktif', 'Laki-laki', 6, 'siswa'),
(58, '5-25019	', 'Herliana Safitri', 'herli', 'herli', '082299715856', 'Jl. Cipinang Muara II Rt. 008/002 No. 40c\r\n', 'Aktif', 'Perempuan', 6, 'siswa'),
(59, '5-25020	', 'Nawal Adhwa Aulia', 'nawal', 'nawal', '081374744346', 'Jl. Y Cipinang Muara II Rt. 005/003 No. 3, Cipinang Muara\r\n', 'Aktif', 'Perempuan', 6, 'siswa'),
(60, '17281999900028', 'Shalsabila Azzahra', 'sabil', 'sabil', '081283747328', 'Jl.TB Simatupang No.20', 'PNS', 'Perempuan', 3, 'guru'),
(61, '3465818181', 'Staff TU', 'staff_tu', '123', '081286431115', 'Depok, Jawa Barat', 'Honorer', 'Laki-laki', 3, 'staff tu'),
(62, '98978', 'falah', 'falahgans', '123123123', '081291817030', 'semanggi', 'PNS', 'Laki-laki', 4, 'staff keuangan'),
(63, '8762381', 'raihan', 'raihan', '123123', '08128557513', 'ciputat', 'PNS', 'Laki-laki', 5, 'staff perpustakaan'),
(64, '1', 'Wali Murid', 'wali', '123', '0929281', 'Ciputat', 'Wali Murid', 'Laki-laki', 2, 'wali murid');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`artikel_id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`galeri_id`),
  ADD UNIQUE KEY `galeri_nama` (`galeri_nama`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kelas_id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`nilai_id`),
  ADD KEY `siswa_id` (`id`),
  ADD KEY `pelajaran_id` (`pelajaran_id`),
  ADD KEY `tahunajaran_id` (`tahun_id`),
  ADD KEY `semester_id` (`semester_id`);

--
-- Indexes for table `pelajaran`
--
ALTER TABLE `pelajaran`
  ADD PRIMARY KEY (`pelajaran_id`);

--
-- Indexes for table `sekolah`
--
ALTER TABLE `sekolah`
  ADD PRIMARY KEY (`sekolah_id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`semester_id`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`tahun_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `kelas_id_2` (`kelas_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `artikel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `galeri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kelas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `nilai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT for table `pelajaran`
--
ALTER TABLE `pelajaran`
  MODIFY `pelajaran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sekolah`
--
ALTER TABLE `sekolah`
  MODIFY `sekolah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `semester_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `tahun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
