<?php
    session_start();
    if (isset($_SESSION['username'])) {
        header('Location: index.php');
    }else{
         $baseurl = "http://".$_SERVER['SERVER_NAME'].dirname($_SERVER["REQUEST_URI"]."?")."/";
?>
<!DOCTYPE html>
<html lang="en">
<!--Head-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Halaman Log in</title> 

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/font/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/isotope.css" media="screen" />
    <link rel="stylesheet" href="assets/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="assets/css/da-slider.css" />
</head>
<!-- Head Ends-->
<!--Body-->
<body>
<header class="header">
        <div class="container">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.php" class="navbar-brand scroll-top logo"><img src="assets/images/logo.png" alt="" style="margin-top:-10px;"> <b>Sistem Informasi Sekolah</b></a>
                </div>
            </nav>
            <!--/.navbar-->
        </div>        
        <!--/.container-->
    </header>

<div class="row">
    <div class="col-sm-4"></div>

    <div class="col-sm-4">
        <h2 class="text-center card-title" style="color: blue;">Log in</h2>
        <form class="form-horizontal" role="form" method="post">
            <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" name="username" placeholder="Username" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12"> 
                    <input type="password" class="form-control" name="password" placeholder="Enter your password" required>
                </div>
            </div>
            <div class="form-group"> 
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-lg btn-primary btn-block" name="login">Login</button>
                </div>
            </div>
                <?php if(isset($error)) : ?>
                    <p style="color: red; font style: italic;">Username atau password salah!</p>
                <?php endif; ?>
            <p align="center">Belum Punya akun? <a href="./registrasi.php">Register</a></p>
        </form>
        <?php require_once('core/login_proses.php'); ?>
    </div>

    <div class="col-sm-4"></div>
</div>
    
    <!--Basic Scripts-->
    <script src="<?php echo $baseurl; ?>dashboard/assets/js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo $baseurl; ?>dashboard/assets/js/bootstrap.min.js"></script>
</body>
<!--Body Ends-->
</html>
<?php } ?>